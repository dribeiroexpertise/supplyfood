json.extract! good, :id, :Expiration_date, :measure_unit_id, :good_type_id, :quantity, :donation_id, :address_id, :created_at, :updated_at
json.url good_url(good, format: :json)
